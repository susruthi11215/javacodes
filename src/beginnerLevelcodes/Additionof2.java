package beginnerLevelcodes;

import java.util.Scanner;

public class Additionof2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("enter number1:");
        int num1 = sc.nextInt();
        System.out.print("enter number2:");
        int num2 = sc.nextInt();
        int sum = num1 + num2;
        System.out.println("sum = " + sum);
    }
}
